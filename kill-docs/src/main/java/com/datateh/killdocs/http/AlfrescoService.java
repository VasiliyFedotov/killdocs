package com.datateh.killdocs.http;

import java.net.*;

import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.entity.*;
import com.datateh.killdocs.entity.*;
import org.apache.http.impl.client.*;
import org.apache.http.client.methods.*;

import java.util.List;

import com.google.gson.Gson;
import org.apache.http.util.EntityUtils;

import java.nio.charset.StandardCharsets;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.utils.URIBuilder;


public class AlfrescoService {
    private String host;
    private String endDate;
    private String user;
    private String password;
    private URI deleteURI;
    private URI searchURI;
    private URI archiveURI;
    private int counter;
    private ArchiveCleanResult archiveCleanResult;

    public AlfrescoService(String host, String endDate, String user, String password) {
        this.host = host;
        this.endDate = endDate;
        this.user = user;
        this.password = password;
        deleteURI = null;
        searchURI = null;
        archiveCleanResult = new ArchiveCleanResult();
    }

    public List<SearchResults> getNodeRefs() {
        if (searchURI == null) {
            createSearchUri();
        }
        HttpGet httpGet = new HttpGet(searchURI);
        setHeader(httpGet, false);

        ResponseHandler<String> responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            System.out.println("Поиск документов, status : " + status);
            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;
        };

        HttpClient client = getHttpClient();
        String responseBody = null;
        try {
            responseBody = client.execute(httpGet, responseHandler);
            ((CloseableHttpClient) client).close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Gson gson = new Gson();
        SearchResponse result = gson.fromJson(responseBody, SearchResponse.class);
        System.out.println("Найдено : " + result.numResults);
        return result.results;
    }

    public void cleanArchive() {
        int index = 0;
        while (archiveCleanResult.isNext()) {
            System.out.println("Очистка корзины, инетраця :\t" + index++);
            try {
                if (archiveURI == null) {
                    createArchiveUri();
                }

                HttpDelete httpDelete = new HttpDelete(archiveURI);
                setHeader(httpDelete, false);
                String responseBody = execute(getHttpClient(), httpDelete);

                Gson gson = new Gson();
                ArchiveResult resp = gson.fromJson(responseBody, ArchiveResult.class);
                if(resp.data != null){
                    List<String> nodes = resp.getData().getNodesType();
                    nodes.stream().forEach(e -> archiveCleanResult.add(e));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("----------------------------- Результат чистки корзины -----------------------------");
        System.out.println("Node no longer exists : " + archiveCleanResult.getLongerExists());
        System.out.println("Node Type : " + archiveCleanResult.getNodeType());
        System.out.println("Unknown row : " + archiveCleanResult.getUnKnown());
    }

    public int deleteNodeRefs(List<String> nodes, List<String> skip) {
        int index = 1;
        counter = 0;
        for (String node : nodes) {
            System.out.print("start " + index + " : " + node + "\t\t");
            if (!delete(node)) {
                skip.add(node);
            }
            index++;
        }
        System.out.println("Удалено " + counter + " документов, пропущено " + (nodes.size() - counter) + " документов");
        return counter;
    }

    private boolean delete(String node) {
        try {
            if (deleteURI == null) {
                createDeleteUri();
            }

            HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(deleteURI);
            setHeader(httpDelete, true);
            String param = "[\"" + node + "\"]";
            StringEntity input = new StringEntity(param, ContentType.APPLICATION_JSON);
            httpDelete.setEntity(input);
            String responseBody = execute(getHttpClient(), httpDelete);

            Gson gson = new Gson();
            DeleteResponse resp = gson.fromJson(responseBody, DeleteResponse.class);

            if (resp.success != null && resp.success.length > 0) {
                counter++;
                System.out.println("[success]");
                return true;
            }
            if (resp.fail != null && resp.fail.length > 0) {
                System.out.println("[fail]");
                return true;
            }
            if (resp.status != null) {
                System.out.println("[status : " + resp.status.code + "]");
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private void setHeader(HttpRequestBase requestBase, Boolean isContent) {
        String auth = user + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
        String authHeader = "Basic " + new String(encodedAuth);
        requestBase.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        if (isContent) {
            requestBase.addHeader("Content-Type", ContentType.APPLICATION_JSON.toString());
        }
    }

    private HttpClient getHttpClient() {
        return HttpClientBuilder.create().build();
    }

    private void createDeleteUri() {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http")
                .setHost(host)
                .setPath("/alfresco/service/lecm/document-removal/document/delete");
        try {
            deleteURI = builder.build();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createArchiveUri() {
        URIBuilder builder = new URIBuilder();
        builder.setScheme("http")
                .setHost(host)
                .setPath("/alfresco/s/api/archive/workspace/SpacesStore");
        try {
            archiveURI = builder.build();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createSearchUri() {
        URIBuilder builder = new URIBuilder();
        String query = "PATH:\"/app:company_home//*\" AND TYPE:\"lecm-document:base\" AND NOT ASPECT:\"rn-template:aspect\" AND @cm\\:modified:[MIN TO \"" + endDate + "\"]";
        builder.setScheme("http")
                .setHost(host)
                .setPath("/alfresco/service/slingshot/node/search")
                .setParameter("lang", "lucene")
                .setParameter("store", "workspace://SpacesStore")
                .setParameter("q", query);
        try {
            searchURI = builder.build();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    private String execute(HttpClient client, HttpRequestBase requestBase) {
        String result = null;
        ResponseHandler<String> responseHandler = response -> {
            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;
        };
        try {
            result = client.execute(requestBase, responseHandler);
            ((CloseableHttpClient) client).close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
