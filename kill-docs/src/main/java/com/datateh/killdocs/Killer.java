package com.datateh.killdocs;

import com.datateh.killdocs.http.AlfrescoService;

import java.util.ArrayList;
import java.util.List;

public class Killer {
    private final int HOST = 0;
    private final int DATE = 1;
    private final int USER = 2;
    private final int PASSWORD = 3;
    private final int DOCUMENTS = 4;
    private final int ARCHIVE = 5;
    private AlfrescoService aService;
    private int iteration;
    private List<String> skip;
    private List<String> nodes;
    private int counter = 0;
    private boolean isArchive;
    private boolean isDocuments;

    public static void main(String[] args) {
        if (args.length == 6) {
            Killer killer = new Killer(args);
            killer.run();
        } else {
            System.out.println("--------\tДля корректной работы необходимо указать 6 параметров\t--------");
            System.out.println("1 - Host (например: 127.0.0.1:8080 или localhost:8080)");
            System.out.println("2 - Дата до которой искать (шаблон: YYYY-MM-DD)");
            System.out.println("3 - Пользователь (например: admin)");
            System.out.println("4 - Пароль (например: 123456)");
            System.out.println("5 - Осуществлять чистку документов(например: (t/true) - включить, (f/false) - выключить)");
            System.out.println("6 - Осуществлять чистку архива (например: (t/true) - включить, (f/false) - выключить)");
        }
    }

    public Killer(String[] param) {
        iteration = 0;
        skip = new ArrayList<>();
        aService = new AlfrescoService(param[HOST], param[DATE], param[USER], param[PASSWORD]);
        isDocuments = checkParam(param[DOCUMENTS]);
        isArchive = checkParam(param[ARCHIVE]);
    }

    private boolean checkParam(String param) {
        switch (param) {
            case "t":
            case "true":
                return true;
            case "f":
            case "false":
                return false;
            default:
                return true;
        }
    }

    public void run() {
        while (isDocuments) {
            System.out.println("Итерация :\t" + iteration++);
            nodes = new ArrayList<>();
            aService.getNodeRefs().forEach(node -> {
                if (!skip.contains(node.nodeRef)) {
                    nodes.add(node.nodeRef);
                }
            });
            if (nodes.size() == 0) {
                break;
            } else {
                System.out.println("Подготовлено к удалению :\t" + nodes.size());
                int localCount = aService.deleteNodeRefs(nodes, skip);
                counter += localCount;
                if (localCount == nodes.size() && localCount < 10)
                    break;
            }
        }
        if(isDocuments){
            System.out.println("Подготовлено к удалению, вторичный прогон :\t" + skip.size());
            counter += aService.deleteNodeRefs(skip, new ArrayList<>());
            System.out.println("Всего было удалено :\t" + counter);
        }
        if(isArchive)
            aService.cleanArchive();
    }
}
