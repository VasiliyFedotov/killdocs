package com.datateh.killdocs.entity;

public class DeleteResponse {
    public String[] fail;
    public String[] success;
    public Status status;
}
