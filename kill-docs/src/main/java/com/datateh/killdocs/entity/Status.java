package com.datateh.killdocs.entity;

public class Status {
    public Integer code;
    public String name;
    public String description;
}
