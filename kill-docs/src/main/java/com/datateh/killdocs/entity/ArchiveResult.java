package com.datateh.killdocs.entity;

public class ArchiveResult {
    public Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
