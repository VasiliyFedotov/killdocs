package com.datateh.killdocs.entity;

import java.util.List;

public class SearchResponse {
    public int numResults;
    public List<SearchResults> results;
}
