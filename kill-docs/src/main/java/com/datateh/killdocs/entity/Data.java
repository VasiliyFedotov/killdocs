package com.datateh.killdocs.entity;

import java.util.ArrayList;
import java.util.List;

public class Data {
    public List<NodeRef> purgedNodes;
    private List<String> pNodes;

    public List<String> getNodesType() {
        pNodes = new ArrayList<>(purgedNodes.size());
        purgedNodes.stream().forEach(nodeRef -> pNodes.add(nodeRef.getType()));
        return pNodes;
    }
}

