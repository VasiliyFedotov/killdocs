package com.datateh.killdocs.entity;

public class ArchiveCleanResult {
    private int longerExists;
    private int nodeType;
    private int unKnown;

    private int oldLongerExists;
    private int oldNodeType;
    private int oldNnKnown;

    public ArchiveCleanResult() {
        this.longerExists = 0;
        this.nodeType = 0;
        this.unKnown = 0;
        this.oldLongerExists = -1;
        this.oldNodeType = -1;
        this.oldNnKnown = -1;
    }

    public void add(String type) {
        if (type.equals("Node Type")) {
            nodeType++;
        } else if (type.equals("Node no longer exists")) {
            longerExists++;
        } else {
            unKnown++;
        }
    }

    public int getLongerExists() {
        return longerExists;
    }

    public int getNodeType() {
        return nodeType;
    }

    public int getUnKnown() {
        return unKnown;
    }

    public boolean isNext() {
        if (longerExists == oldLongerExists)
            if (nodeType == oldNodeType)
                if (unKnown == oldNnKnown)
                    return false;
        oldLongerExists = longerExists;
        oldNodeType = nodeType;
        oldNnKnown = unKnown;
        return true;
    }
}
